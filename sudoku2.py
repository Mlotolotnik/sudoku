import random

class sudoku:
    def __init__(self):
        self.initialize_with_zero()
    def initialize_with_zero(self):
        self.board = [[0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0]]
    def initialize_with_index(self):
        for a in range(9):
            for b in range(9):
                self.board[a][b] = a * 9 + b
    def initialize_with_correct_values(self):
        self.board = [[1,2,3,7,8,9,4,5,6],
                        [4,5,6,1,2,3,7,8,9],
                        [7,8,9,4,5,6,1,2,3],
                        [3,1,2,9,7,8,6,4,5],
                        [6,4,5,3,1,2,9,7,8],
                        [9,7,8,6,4,5,3,1,2],
                        [2,3,1,8,9,7,5,6,4],
                        [5,6,4,2,3,1,8,9,7],
                        [8,9,7,5,6,4,2,3,1]]
    def print_board(self):
        print()
        print(self.board[0][0],self.board[0][1],self.board[0][2]," ",self.board[0][3],self.board[0][4],self.board[0][5]," ",self.board[0][6],self.board[0][7],self.board[0][8])
        print(self.board[1][0],self.board[1][1],self.board[1][2]," ",self.board[1][3],self.board[1][4],self.board[1][5]," ",self.board[1][6],self.board[1][7],self.board[1][8])
        print(self.board[2][0],self.board[2][1],self.board[2][2]," ",self.board[2][3],self.board[2][4],self.board[2][5]," ",self.board[2][6],self.board[2][7],self.board[2][8])
        print()
        print(self.board[3][0],self.board[3][1],self.board[3][2]," ",self.board[3][3],self.board[3][4],self.board[3][5]," ",self.board[3][6],self.board[3][7],self.board[3][8])
        print(self.board[4][0],self.board[4][1],self.board[4][2]," ",self.board[4][3],self.board[4][4],self.board[4][5]," ",self.board[4][6],self.board[4][7],self.board[4][8])
        print(self.board[5][0],self.board[5][1],self.board[5][2]," ",self.board[5][3],self.board[5][4],self.board[5][5]," ",self.board[5][6],self.board[5][7],self.board[5][8])
        print()
        print(self.board[6][0],self.board[6][1],self.board[6][2]," ",self.board[6][3],self.board[6][4],self.board[6][5]," ",self.board[6][6],self.board[6][7],self.board[6][8])
        print(self.board[7][0],self.board[7][1],self.board[7][2]," ",self.board[7][3],self.board[7][4],self.board[7][5]," ",self.board[7][6],self.board[7][7],self.board[7][8])
        print(self.board[8][0],self.board[8][1],self.board[8][2]," ",self.board[8][3],self.board[8][4],self.board[8][5]," ",self.board[8][6],self.board[8][7],self.board[8][8])
        print()
        pass
    
    def is_line_correct(self, num_line, num_col, value):
        for x in range(9):
            if num_col == x:
                continue
            else:
                if self.board[num_line][x] == value:
                    return False
        return True
    def is_column_correct(self, num_line, num_col,value):
        for x in range(9):
            if num_line == x:
                continue
            else:
                if self.board[x][num_col] == value:
                    return False
        return True
        
    def is_group_correct(self, num_line, num_col, value):  
        if num_line <= 2:
            lin_range_from = 0
            lin_range_to = 2
            #print("Lin range set from:", lin_range_from, "to", lin_range_to)
        elif num_line <= 5:
            lin_range_from = 3
            lin_range_to = 5
            #print("Lin range set from:", lin_range_from, "to", lin_range_to)
        else:
            lin_range_from = 6
            lin_range_to = 8
            #print("Lin range set from:", lin_range_from, "to", lin_range_to)
        if num_col <= 2:
            col_range_from = 0
            col_range_to = 2
            #print("Col range set from:", col_range_from, "to", col_range_to)
        elif num_col <= 5:
            col_range_from = 3
            col_range_to = 5
            #print("Col range set from:", col_range_from, "to", col_range_to)
        else:
            col_range_from = 6
            col_range_to = 8
            #print("Col range set from:", col_range_from, "to", col_range_to)

        for lin in range(lin_range_from, lin_range_to + 1):
            for col in range(col_range_from, col_range_to + 1):
                if num_line == lin and num_col == col:
                    #print("Box for check the same as the place of input - skipping this one.")
                    continue
                else:
                    if self.board[lin][col] == value:
                        #print("Value in box lin:", lin, "col:", col, "have the same value as input value:", value)
                        return False
        return True

    def is_box_correct(self, num_line, num_col,value):
        if self.is_line_correct(num_line, num_col, value) and self.is_column_correct(num_line, num_col, value) and self.is_group_correct(num_line, num_col, value):
            return True
        else:
            return False
    def is_board_correct(self):
        for lin in range(9):
            for col in range(9):
                if self.is_box_correct(lin, col, self.board[lin][col]):
                    continue
                else:
                    return False
        return True

    def shuffle_lines(self, number_of_switches):
        for i in range(number_of_switches):
            lin1 = 0
            lin2 = 0
            while lin1 == lin2:
                lin1 = random.randint(0,8)
                lin2 = random.randint(0,8)
            self.switch_lines(lin1, lin2)
        
    def switch_lines(self, num_of_line1, num_of_line2):
        temp_line = [0,0,0,0,0,0,0,0,0]
        for i in range(9):
            temp_line[i] = self.board[num_of_line1][i]
        for i in range(9):
            self.board[num_of_line1][i] = self.board[num_of_line2][i]
        for i in range(9):
            self.board[num_of_line2][i] = temp_line[i]
            
    def shuffle_columns(self, number_of_switches):
        for i in range(number_of_switches):
            col1 = 0
            col2 = 0
            while col1 == col2:
                col1 = random.randint(0,8)
                col2 = random.randint(0,8)
            self.switch_columns(col1, col2)
    def switch_columns(self, num_of_column1, num_of_column2):
        temp_column = [0,0,0,0,0,0,0,0,0]
        for i in range(9):
            temp_column[i] = self.board[i][num_of_column1]
        for i in range(9):
            self.board[i][num_of_column1] = self.board[i][num_of_column2]
        for i in range(9):
            self.board[i][num_of_column2] = temp_column[i]
    def shufle_lines_and_columns(self, number_of_times):
        self.shuffle_columns(number_of_times)
        self.shuffle_lines(number_of_times)

table = sudoku()
table.initialize_with_correct_values()

table.shufle_lines_and_columns(10)
counter = 0
while not table.is_board_correct():
    table.shufle_lines_and_columns(1)
    counter += 1
table.print_board()
print("Liczba poprawkowych losowań:", counter)




